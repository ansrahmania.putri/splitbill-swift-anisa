//
//  ContactStruct.swift
//  AddDeleteCell
//
//  Created by ADA-NB187 on 13/12/22.
//

import Foundation

struct ContactStruct {
    let givenName: String
    let familyName: String
    let number: String
}
