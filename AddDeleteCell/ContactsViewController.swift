//
//  ContactsViewController.swift
//  AddDeleteCell
//
//  Created by ADA-NB187 on 13/12/22.
//

import UIKit
import Contacts
import ContactsUI


class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CNContactPickerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnDone: UIButton!
    
    var contactStore = CNContactStore()
    var contacts = [ContactStruct]()
    var items = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        contactStore.requestAccess(for: .contacts) {
            (success, errror) in
            if success {
                print("Authorization Successfull")
            }
        }
        fetchContacts()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        let contacttoDisplay = contacts[indexPath.row]
        cell.textLabel?.text = contacttoDisplay.givenName + " " + contacttoDisplay.familyName
        cell.detailTextLabel?.text = contacttoDisplay.number

        //select contacts
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    func fetchContacts() {
        let key = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        try! contactStore.enumerateContacts(with:
                                        request) {
            (contact, stoppingPointer) in
            let name = contact.givenName
            let familyName = contact.familyName
            let number = contact.phoneNumbers.first?.value.stringValue
            
            let contactToAppend = ContactStruct (givenName: name, familyName: familyName, number: number!)
            contacts.append(contactToAppend)
        }
        tableView.reloadData()
        print(contacts.first?.givenName)
    }
    
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        contacts.removeAll()
        if let selectedContact = tableView.indexPathsForSelectedRows {
            for iPath in selectedContact {
                contacts.append(contacts[iPath.row])
            }
            print("----You have selected contacts----")
            for contact in contacts {
                print(contact)
            }
        }
    }
    
}

