//
//  ViewController.swift
//  AddDeleteCell
//
//  Created by ADA-NB187 on 12/12/22.
//

import UIKit
import DropDown

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnDropDown: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var txtInput2: UITextField!
    @IBOutlet weak var txtInput3: UITextField!
    @IBOutlet weak var txtInput4: UITextField!
    
    @IBOutlet weak var lblDropDown: UILabel!
    
    
    var stringArr = [String]()
    
    let dropDown = DropDown()
    let choiceValues = ["Bagi Sama Rata", "Bagi Sesuai Pesanan"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
//
//        btnCreateSB.addTarget(self, action: #selector(btnCreateSBTapped), for: .touchUpInside)
//        btnCreateSB.layer.cornerRadius = 5
        btnDetails.addTarget(self, action: #selector(btnDetailsTapped), for: .touchUpInside)
        
        lblDropDown.text = "Pilih Tipe Pembagian"
        dropDown.anchorView = btnDropDown
        dropDown.dataSource = choiceValues
        dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y: -(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.direction = .bottom
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Tipe pembagian yang dipilih: \(item) at index: \(index)")
            self.lblDropDown.text = choiceValues[index]

        }

    }
    
    @objc func btnCreateSBTapped()
    {
       
    }
    
    @objc func btnDetailsTapped() {
        let contactsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
        self.navigationController?.pushViewController(contactsViewController, animated: true)
    }
    
    @IBAction func btnDropDownTapped(_ sender: Any) {
        dropDown.show()
    }
    

    @IBAction func onClickAddButton(_ sender: Any) {
        if let txt = txtInput.text,
           let txt2 = txtInput2.text,
           let txt3 = txtInput3.text,
           let txt4 = txtInput4.text
        {
            let qty = Int(txt2) ?? 0
            let amount = Int(txt3) ?? 0
            let total = qty * amount
            print(total)
            let txtCombine = txt + ", " + txt2 + ", " + txt3 +  ", " + String(total) + ", " + txt4
            let txtArr = txtCombine.components(separatedBy: ", ")
            print(txtCombine)
            print(txtArr)
            
            stringArr.append(
                "Item: \(txtArr[0])\n" +
                "Quantity: \(txtArr[1])\n" +
                "Amount: Rp. \(txtArr[2])\n" +
                "Total Amount: Rp. \(txtArr[3])\n" +
                "Person: \(txtArr[4])")
            let indexPath = IndexPath(row: stringArr.count - 1, section: 0)
            tableView.beginUpdates()
            tableView.insertRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            txtInput.text = ""
            txtInput2.text = ""
            txtInput3.text = ""
            txtInput4.text = ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditTableViewCell", for: indexPath)
        cell.textLabel?.text = stringArr [indexPath.row]
        cell.textLabel?.numberOfLines = 0

        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") {
            (action, view, completionHandler) in
            self.stringArr.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
            completionHandler(true)
                                  
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}
